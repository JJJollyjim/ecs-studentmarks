# Sample Output of `parse_marks`
```rust
[
    Paper {
        full_name: "ENGR110 - Engineering Modelling and Design (2017 T2)",
        assessments: [
            Assessment {
                name: "Assignment_1",
                final_mark: Some(Mark {
                    question_name: "Final",
                    mark: 100,
                    available: 100,
                }),
                marks: [
                    Mark {
                        question_name: "Core",
                        mark: 19,
                        available: 19,
                    },
                    Mark {
                        question_name: "Completion",
                        mark: 15,
                        available: 15,
                    },
                    Mark {
                        question_name: "Challenge",
                        mark: 4,
                        available: 4,
                    },
                ],
            },
            Assessment {
                name: "Tutorial_1",
                final_mark: Some(Mark {
                    question_name: "Final",
                    mark: 100,
                    available: 100,
                }),
                marks: [
                    Mark {
                        question_name: "Done",
                        mark: 100,
                        available: 100,
                    },
                ],
            },
            Assessment {
                name: "Tutorial_3",
                final_mark: Some(Mark {
                    question_name: "Final",
                    mark: 100,
                    available: 100,
                }),
                marks: [
                    Mark {
                        question_name: "Done",
                        mark: 100,
                        available: 100,
                    },
                ],
            },
        ],
    },
    Paper {
        full_name: "ENGR122 - Engineering Mathematics with Calculus (2017 T2)",
        assessments: [
            Assessment {
                name: "Lab_1",
                final_mark: Some(Mark {
                    question_name: "Final",
                    mark: 96,
                    available: 105,
                }),
                marks: [
                    Mark {
                        question_name: "CORE 1",
                        mark: 10,
                        available: 10,
                    },
                    Mark {
                        question_name: "CORE 2",
                        mark: 5,
                        available: 10,
                    },
                    Mark {
                        question_name: "CORE 3",
                        mark: 10,
                        available: 10,
                    },
                    Mark {
                        question_name: "CORE 4",
                        mark: 13,
                        available: 15,
                    },
                    Mark {
                        question_name: "COMPLETION 1",
                        mark: 10,
                        available: 10,
                    },
                    Mark {
                        question_name: "COMPLETION 2",
                        mark: 10,
                        available: 10,
                    },
                    Mark {
                        question_name: "COMPLETION 3",
                        mark: 14,
                        available: 15,
                    },
                    Mark {
                        question_name: "COMPLETION 4",
                        mark: 10,
                        available: 10,
                    },
                    Mark {
                        question_name: "CHALLENGE 1",
                        mark: 5,
                        available: 5,
                    },
                    Mark {
                        question_name: "CHALLENGE 2",
                        mark: 5,
                        available: 5,
                    },
                ],
            },
        ],
    },
    Paper {
        full_name: "ENGR123 - Engineering Mathematics with Logic and Statistics (2017 T2)",
        assessments: [
            Assessment {
                name: "Lab_1",
                final_mark: Some(Mark {
                    question_name: "Final",
                    mark: 95,
                    available: 100,
                }),
                marks: [
                    Mark {
                        question_name: "CORE",
                        mark: 50,
                        available: 50,
                    },
                    Mark {
                        question_name: "COMPLETION",
                        mark: 25,
                        available: 30,
                    },
                    Mark {
                        question_name: "CHALLENGE",
                        mark: 20,
                        available: 20,
                    },
                ],
            },
        ],
    },
    Paper {
        full_name: "ENGR142 - Engineering Physics for Electronics and Computer Systems (2017 T2)",
        assessments: [],
    },
];
```
