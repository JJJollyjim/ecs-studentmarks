use scraper::{Html, Selector, ElementRef};
use scraper::element_ref::Text;
use std;
use regex::Regex;

#[derive(Debug)]
pub enum ParseError {
    PaperNameError,
    AssessmentNameError,
    MarkTableError,
    XSlashYError,
    ParseFloatError(std::num::ParseFloatError),
}

impl From<std::num::ParseFloatError> for ParseError {
    fn from(e: std::num::ParseFloatError) -> Self {
        ParseError::ParseFloatError(e)
    }
}

#[derive(Debug)]
pub struct Paper {
    full_name: String,
    assessments: Vec<Assessment>,
}

#[derive(Debug)]
pub struct Assessment {
    name: String,
    final_mark: Option<Mark>,
    marks: Vec<Mark>,
}

#[derive(Debug, Clone)]
pub struct Mark {
    question_name: String,
    mark: f64,
    available: f64,
    // TODO comments
}

pub fn parse_marks(html: String) -> Result<Vec<Paper>, ParseError> {
    lazy_static! {
        static ref PAPER_SELECTOR: Selector = Selector::parse("div.tab-pane").unwrap();
    }

    let doc = Html::parse_document(&html);
    doc.select(&PAPER_SELECTOR)
        .map(parse_paper)
        .collect::<Result<Vec<_>, _>>()
}

pub fn parse_paper(pane: ElementRef) -> Result<Paper, ParseError> {
    lazy_static! {
        static ref NAME_SELECTOR: Selector = Selector::parse("h3").unwrap();
        static ref ASSESSMENT_SELECTOR: Selector = Selector::parse("div.panel").unwrap();
    }

    let name = pane.select(&NAME_SELECTOR)
        .next()
        .ok_or(ParseError::PaperNameError)?
        .text()
        .join();
    let assessments = pane.select(&ASSESSMENT_SELECTOR)
        .map(parse_assessment)
        .collect::<Result<Vec<_>, _>>()?;

    Ok(Paper {
        full_name: name,
        assessments: assessments,
    })
}

pub fn parse_assessment(panel: ElementRef) -> Result<Assessment, ParseError> {
    lazy_static! {
        static ref NAME_SELECTOR: Selector = Selector::parse("div.panel-heading > h4").unwrap();
        static ref MARK_SELECTOR: Selector = Selector::parse("table.table-nonfluid tr").unwrap();
    }

    let name = panel
        .select(&NAME_SELECTOR)
        .next()
        .ok_or(ParseError::AssessmentNameError)?
        .text()
        .join();
    let all_marks = panel
        .select(&MARK_SELECTOR)
        .skip(1)
        .map(parse_mark)
        .collect::<Result<Vec<_>, _>>()?;

    let (final_marks, marks): (Vec<Mark>, Vec<Mark>) = all_marks.into_iter().partition(|mark| {
        mark.question_name == "Final"
    });

    let final_mark: Option<Mark> = final_marks.get(0).map(Mark::clone);

    Ok(Assessment {
        name: name,
        final_mark: final_mark,
        marks: marks,
    })
}

pub fn parse_mark(row: ElementRef) -> Result<Mark, ParseError> {
    lazy_static! {
        static ref X_SLASH_Y_RE: Regex = Regex::new(r"([0-9]+(\.[0-9]+)?)\s*/\s*([0-9]+(\.[0-9]+)?)").unwrap();
    }

    let mut children = row.children();


    let question = ElementRef::wrap(children.next().ok_or(ParseError::MarkTableError)?)
        .ok_or(ParseError::MarkTableError)?
        .text()
        .join();
    let x_slash_y = ElementRef::wrap(children.next().ok_or(ParseError::MarkTableError)?)
        .ok_or(ParseError::MarkTableError)?
        .text()
        .join();

    let captures = X_SLASH_Y_RE.captures(&x_slash_y).ok_or(
        ParseError::XSlashYError,
    )?;

    let x = captures
        .get(1)
        .ok_or(ParseError::XSlashYError)?
        .as_str()
        .parse()?;
    let y = captures
        .get(3)
        .ok_or(ParseError::XSlashYError)?
        .as_str()
        .parse()?;

    Ok(Mark {
        question_name: question,
        mark: x,
        available: y,
    })

}

trait TextExt {
    fn join(self: Self) -> String;
}

impl<'a> TextExt for Text<'a> {
    fn join(self: Self) -> String {
        let mut result = String::new();
        for t in self {
            result.push_str(t);
        }
        result
    }
}


#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;
    use super::*;

    #[test]
    fn parses_successfully() {
        let mut html = String::new();
        let _ = File::open("fixtures/studentmarks.html")
            .unwrap()
            .read_to_string(&mut html);

        assert!(parse_marks(html).is_ok());
    }
}
