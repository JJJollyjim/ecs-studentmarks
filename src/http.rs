use reqwest::{self, Client, RedirectPolicy};
use reqwest::header::{self, Header};
use std::io::Read;

#[derive(Debug)]
pub enum HttpError {
    AuthError,
    UnexpectedResponseError,
    CookieParseError,
    ReqwestError(reqwest::Error),
}

impl From<reqwest::Error> for HttpError {
    fn from(e: reqwest::Error) -> Self {
        HttpError::ReqwestError(e)
    }
}

pub fn make_client() -> Result<Client, reqwest::Error> {
    Client::builder()?.redirect(RedirectPolicy::none()).build()
}

pub fn get_marks_page(client: Client, auth: &str) -> Result<Box<Read>, HttpError> {
    let mut cookie_raw = String::from("Cookie: ");
    cookie_raw.push_str(auth);
    let cookie = header::Cookie::parse_header(&cookie_raw.into()).or(Err(
        HttpError::CookieParseError,
    ))?;

    let resp = client
        .get("https://ecs.victoria.ac.nz/cgi-bin/studentmarks")
        .unwrap()
        .header(cookie)
        .send()?;

    if resp.status() == reqwest::StatusCode::Found {
        Err(HttpError::AuthError)
    } else if resp.status().is_success() {
        Ok(Box::new(resp))
    } else {
        Err(HttpError::UnexpectedResponseError)
    }
}
