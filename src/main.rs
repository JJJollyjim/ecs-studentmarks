#[macro_use] extern crate lazy_static;
#[macro_use] extern crate clap;
extern crate reqwest;
extern crate scraper;
extern crate regex;

mod http;
mod parser;

fn main() {
    let args = clap_app!(studentmarks =>
                         (version: crate_version!())
                         (author: "Jamie McClymont <jamie@kwiius.com>")
                         (about: "Gets marks from the ecs studentmarks site")
                         (@group auth =>
                          (@attributes +required)
                          (@arg COOKIESTR: -c --cookie +takes_value "The value of the Cookie header (specifically the ECS[ipaddr] cookie is required)")
                          (@arg USERPASS: -u --userpass +takes_value "ECS login in the form username:password") // TODO support user and pass args, with pass not supplied -> take by stdin
                         )
    ).get_matches();

    let cookie = match (args.value_of("COOKIESTR"), args.value_of("USERPASS")) {
        (Some(cs), _) => cs,
        (_, Some(_up)) => panic!("Authentication is not yet implemented"),
        _ => unreachable!(),
    };

    let client = http::make_client().unwrap();
    let mut res = http::get_marks_page(client, cookie).unwrap();

    let mut resstr = String::new();
    let _ = res.read_to_string(&mut resstr);

    println!("{:#?}", parser::parse_marks(resstr));
}
